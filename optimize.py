from __future__ import print_function
from pv import *
from paths import *
import pandas as pd
import numpy as np
# from gams import *
from pandas import DataFrame, Series
import os
import sys
import logging
import random
from input import *
from setter import *
from gurobipy import *
from battery import *

pd.set_option('display.expand_frame_repr', False)

__all__=['Optimizer']
class Optimizer(InputSetter,Battery):
    def __init__(self, day=1, time=24, loadRow=0, **kwargs):  # todo Enable user to input parameters using kwargs
        InputSetter.__init__(self)
        Battery.__init__(self, kwargs.get('Battery', None))
        self.day = day
        self.testArb=False
        self.timeDuration = time
        self.loadRow = loadRow
        self.__fixedFIT = .123 #eur/kwh
        self.__electricityTariff = .29  # eur/kWh
        self.__fixedElectricity = .0563  # eur/kWh
        self.__taxes = .0371  # eur/KWh Konzessionsabgabe & Stromsteuer
        self.__EEG=0.0688+.008
        self.__RTPStatus = True
        self.__vFITStatus = False
        self.__capacityStatus = False
        self.optimizationStatus=None
        self.arbitrageState=None
        self.c=self.__calcElecConstant()
        self.alpha=self.__calcEEGratio()
        self.beta=self.__calcFeedinRatio()
        # self.calculateRetailElectricity()
        # self.calculateFeedInTariff()
        # self._Autarky=None
        # Inputs.timeDuration=self.duration
        # Inputs.day=self.day
        # Inputs.loadRow=self.loadRow

    # Status of optimization settings
    @property
    def RTPStatus(self):
        return self.__RTPStatus

    @RTPStatus.setter
    def RTPStatus(self, rtp):
        if type(rtp)!=bool:
            raise ValueError("RTP Status can only be True or False")
        else:
            assert type(rtp) == bool
            self.__RTPStatus = rtp
            logging.info('RTP Status Changed to {}'.format(str(rtp)))
            self.calculateRetailElectricity()


    @property
    def capacityStatus(self):
        return self.__capacityStatus

    @capacityStatus.setter
    def capacityStatus(self, cap):
        if type(cap)!=bool:
            raise ValueError("Capacity Status can only be True or False")
        else:
            assert type(cap) == bool
            self.__capacityStatus = cap
            logging.info('Capacity Status Changed to {}'.format(str(cap)))
            self.calculateRetailElectricity()


    @property
    def vFITStatus(self):
        return self.__vFITStatus

    @vFITStatus.setter
    def vFITStatus(self, fit):
        if type(fit)!=bool:
            raise ValueError("FIT Status can only be True or False")
        else:
            self.__vFITStatus = fit
            self.calculateFeedInTariff()
            logging.info('Feed in Status Changed to {}'.format(str(fit)))


    @property  # Variable can only be returned, not set
    def FIT(self):
        return self.calculateFeedInTariff()

    @property
    def fixedElectricity(self):
        return self.__fixedElectricity
    @fixedElectricity.setter
    def fixedElectricity(self,f):
        self.__fixedElectricity=f
        return self.__fixedElectricity
    @property
    def fixedFIT(self):  # value of constant FIT
        return self.__fixedFIT

    @fixedFIT.setter
    def fixedFIT(self, fixedF):
        self.__fixedFIT = fixedF
        self.calculateFeedInTariff()
        logging.info("Fixed Feed In Tariff Set to {} ".format(fixedF))

    @property
    def retailElectricityTariff(self):
        return self.calculateRetailElectricity()


    @property
    def networkCharge(self):
        return self.__networkCharge

    @networkCharge.setter  # ability to change volumetric network charge
    def networkCharge(self, network):
        self.__networkCharge = network
        logging.info("Network Charge per kWh Updated to {}".format(network))


    def __calcElecConstant(self):
        totalAvgLoad = self.LoadListGet(day=1, duration=8760, loadRow=100)
        totalPrice = self.PricesListGet(day=1, duration=8760)
        c = (self.fixedElectricity *sum(totalAvgLoad)-np.dot(totalAvgLoad,totalPrice))/sum(totalAvgLoad) #constant added of price
        return c

    def __calcEEGratio(self):
        totalAvgLoad = self.LoadListGet(day=1, duration=8760, loadRow=100)
        totalPrice = self.PricesListGet(day=1, duration=8760)
        c=self.__calcElecConstant()
        alpha = self.__EEG * sum(totalAvgLoad) / np.dot((totalPrice + c), totalAvgLoad)
        return alpha

    def __calcFeedinRatio(self):
        totalPrice = self.PricesListGet(day=1, duration=8760)
        totalPV = self.PvGenListGet(day=1, duration=8760)
        c =self.__calcElecConstant()
        beta=self.__fixedFIT*sum(totalPV)/(np.dot((totalPrice+c),totalPV))
        return beta


    # Calculate Feed in Tariff and Electricity Retail Price based on regime
    def calculateFeedInTariff(self):

        if self.vFITStatus:
            logging.warning(" Variable FIT Set")
            self.__FIT = self.beta * (self.priceList+self.c)  # coefficient obtained by dividing total feed in remuneration to realtime prices times production
            self.__FIT = np.array(self.__FIT)
        else:
            self.__FIT = [self.__fixedFIT] * self.timeDuration
        if self.testArb:
            self.__FIT=self.priceList

        return self.__FIT

    def calculateRetailElectricity(self, capacity=None, RTP=None,vFIT=None):
        if capacity == None:
            capacity = self.capacityStatus
        if RTP == None:
            RTP = self.RTPStatus
        if vFIT==None:
            vFIT=self.vFITStatus
        if capacity:
            self.networkCharge = 0
            self.fixedCapacity = 204*1.19
            logging.info("No Network Charges Imposed")
        else:
            self.networkCharge = .0569  # ct per kWh
            self.fixedCapacity = 66*1.19  # eur per KW per year
            logging.info("Volumetric Network Charges Imposed")

        if vFIT and RTP:
            self.__EEG = self.alpha* (self.priceList+self.c)  # 2.3 calculated by dividing total eeg umlage by realtime prices and load
        else:
            self.__EEG = .0688+.008  # eur/kWh

        if RTP:
            self.elecBasePrice=(self.priceList+self.c)
            logging.info("Real Time Pricing for Day {} and Time Duration {} Set ".format(self.day, self.timeDuration))
        else:
            fixedPrices = [self.fixedElectricity] * self.timeDuration
            fixedPrices = np.array(fixedPrices)
            self.elecBasePrice = fixedPrices
            logging.info("Constant Prices for day {} and Time Duration {}".format(self.day, self.timeDuration))

        total = self.__EEG + self.__taxes + self.elecBasePrice + self.networkCharge # networkCharge is zero if case is capacity only
        total *= 1.19  # VAT
        if self.testArb:
            total=self.priceList
        return total

    def optimize(self, RTPstatus=None, FITstatus=None, Capacitystatus=None):
        if RTPstatus == None:
            pass
        else:
            self.RTPStatus = RTPstatus
        if FITstatus == None:
            pass
        else:
            self.vFITStatus = FITstatus
        if Capacitystatus == None:
            pass
        else:
            self.capacityStatus = Capacitystatus

        if not self.RTPStatus and not self.vFITStatus:
            return self.__BAU()
        else:
            return self.__optimizerDispatch()



    def calculateBatteryStateBinary(self, energyStorage):
        energyStorage = np.array(energyStorage)
        energyStorage = np.insert(energyStorage, 0, self.initialBatteryCapacity)
        stateDiff = np.diff(energyStorage)
        for i in range(len(stateDiff)):
            stateDiff[i]=round(stateDiff[i],3)
            if stateDiff[i]>0:
                stateDiff[i]=1
            elif stateDiff[i]<-self.selfDischarge*self.maximumBatteryCapacity*1.05:
                stateDiff[i]=-1
            else:
                stateDiff[i]=0

        return stateDiff

    def optimizeArbitrage(self):
        '''


            Function takes Maximum Charge Capacity, Max Discharge Capacity, BatterySize, number of hours and
            a random number generator  and price curve as input

            returns maximum revenue along with hourly Energy dispatch from grid,Energy dispatch to grid and battery state

            Complete foresight, linear optimization done with GUROBI SOLVER



            '''
        self.arbitrageState=True
        model = Model("Arbitrage")  # Create Gurobi Model
        # For each seedNumber price P generated will be the same
        prices=self.priceList
        N = len(prices)
        model.setParam('OutputFlag', 0)
        ECharge, EDischarge, EStorage = {}, {}, {}  # Intialize Constraint Dictionary
        # All Efficiencies are taken with reference to the battery. if battery discharges 1kwh, this means it actually gives
        # etaDischarge*1kwh to the grid...if battery charges by 1 kwh, this means it took 1/etacharge from the grid/pv
        for j in range(N):  # fills constraint dictionary along with lower and upper bounds
            ECharge[j] = model.addVar(vtype='C', lb=0, ub=self.maximumChargeDischargeCapacity / self.chargeEfficiency,
                                      name="ECharge[%s]" % j)
            EDischarge[j] = model.addVar(vtype='C', lb=0, ub=self.maximumChargeDischargeCapacity * self.dischargeEfficiency,
                                         name="EDischarge[%s]" % j)
            EStorage[j] = model.addVar(vtype='C', lb=0, ub=self.maximumBatteryCapacity, name="EStorage[%s]" % j)
        model.update()
        # EDischarge and Echarge are directly to the grid
        # sets objective function
        model.setObjective(sum(EDischarge[j] * prices[j] - (ECharge[j]) * prices[j] for j in range(N)),
                           GRB.MAXIMIZE)
        for i in range(N):  # Adding constraints for length of N
            if i == 0:
                model.addConstr(
                    EStorage[i] - 0 * self.maximumBatteryCapacity * (1 - self.selfDischarge) - ECharge[i] * self.chargeEfficiency +
                    EDischarge[
                        i] / self.dischargeEfficiency == 0)
            else:
                model.addConstr(
                    EStorage[i] - EStorage[i - 1] * (1 - self.selfDischarge) - ECharge[i] * self.chargeEfficiency + EDischarge[
                        i] / self.dischargeEfficiency == 0)
        model.update()
        model.optimize()

        EfromGrid, EtoGrid, BatteryState = [], [], []

        # data wrangling to extract solution. Pretty ugly
        for i in range(N):
            vars = model.getVarByName("ECharge[%s]" % i)
            EfromGrid.append(vars.x)
            vars = model.getVarByName("EDischarge[%s]" % i)
            EtoGrid.append(vars.x)
            vars = model.getVarByName("EStorage[%s]" % i)
            BatteryState.append(vars.x)

        self.energyStorageArbitrage = BatteryState
        self.energyToGrid = EfromGrid
        self.energyFromGrid = EtoGrid
        ans = DataFrame({"Prices": prices,
                         'Battery State (kW)': BatteryState,
                         'Energy from the grid (kW)': EfromGrid,
                         'Energy into the grid (kW)': EtoGrid}, )
        ans = ans.round(2)
        ans = ans[['Prices', 'Battery State (kW)', 'Energy from the grid (kW)', 'Energy into the grid (kW)']]
        self.numOfCyclesArb=self.batteryCountsArb()
        return ans
        # return ans, model.objVal  # function returns results as DataFrame and the value of objective function

    def __BAU(self):
        logging.info(
            "Business as Usual: Day {}, Time Duration {}, PV Size {} ".format(self.day, self.timeDuration,
                                                                              self.pvSize))
        self.optimizationStatus = 1

        if self.capacityStatus:
            self.optimizationState='BAU Capacity'
        else:
            self.optimizationState='BAU Volumetric'

        length = min(len(self.pvGenList),
                     len(self.loadList))  # in case the inputs  are not the same length, use the smaller.
        battState, energyFromGrid, energyToGrid, cases = [], [], [], []  # Create Return Variables
        Xi = self.pvGenList[:length] - self.loadList[:length]
        battStateATBeg = []
        # All Efficiencies are taken with reference to the battery. if battery discharges 1kwh, this means it actually gives
        # etaDischarge*1kwh to the grid...if battery charges by 1 kwh, this means it took 1kwh/etacharge from the grid/pv
        batteryState=self.initialBatteryCapacity
        for item in Xi:
            battAtBeg = batteryState* (1 - self.selfDischarge)
            batteryState *= (1 - self.selfDischarge)
            if item <= 0:
                EtoGrid = 0
                if abs(item) <= min(batteryState, self.maximumChargeDischargeCapacity) * self.dischargeEfficiency:
                    batteryState = batteryState - (abs(item) / self.dischargeEfficiency)
                    EfromGrid = 0
                elif abs(item) > min(batteryState, self.maximumChargeDischargeCapacity) * self.dischargeEfficiency:
                    EfromGrid = abs(item) - min(batteryState,
                                                self.maximumChargeDischargeCapacity) * self.dischargeEfficiency
                    batteryState = batteryState - (
                        min(batteryState, self.maximumChargeDischargeCapacity))
            else:
                EfromGrid = 0
                if item >= min((self.maximumBatteryCapacity - batteryState),
                               self.maximumChargeDischargeCapacity) / self.chargeEfficiency:
                    EtoGrid = item - min((self.maximumBatteryCapacity - batteryState),
                                         self.maximumChargeDischargeCapacity) / self.chargeEfficiency
                    batteryState = batteryState + min((self.maximumBatteryCapacity - batteryState),
                                                      self.maximumChargeDischargeCapacity)
                else:
                    batteryState = batteryState + item * self.chargeEfficiency
                    EtoGrid = 0

            battState.append(batteryState)
            energyFromGrid.append(EfromGrid)
            energyToGrid.append(EtoGrid)
            battStateATBeg.append(battAtBeg)
        # Calculating battery cycle, defined as total charge and discharge of 2*battSize
        # numOfCycles = 0
        # z = 0
        # for i in difference:
        #     z += abs(i)
        #     if z >= 2 * self.maximumBatteryCapacity:
        #         z = z - 2 * self.maximumBatteryCapacity
        #         numOfCycles += 1

        ans = DataFrame({'load (kW)': self.loadList,
                         'PV Generation': self.pvGenList,
                         'Battery State (kW)': battState,
                         'Energy from the grid (kW)': energyFromGrid,
                         'Energy into the grid (kW)': energyToGrid
                            , 'Bat at beg': battStateATBeg},
                        )
        ans = ans.round(2)
        ans = ans[
            ['load (kW)', 'PV Generation', 'Battery State (kW)', 'Energy from the grid (kW)',

             'Energy into the grid (kW)', 'Bat at beg']]
        energyToGrid=np.array(energyToGrid)
        energyFromGrid=np.array(energyFromGrid)

        revenue = np.dot(self.FIT,energyToGrid) - np.dot(self.retailElectricityTariff,energyFromGrid)+self.FIT[0]*batteryState

        self.energyToGrid = energyToGrid
        self.energyFromGrid = energyFromGrid
        self.energyStorage = battState
        self.revenue = revenue
        self.referenceRevenue = np.dot(-self.retailElectricityTariff, self.loadList)
        self.directUse=self.pvGenList-energyToGrid
        self.directUse=sum(self.directUse)-batteryState


        return ans
    def __optimizerDispatch(self):
        self.optimizationStatus = 2
        if self.capacityStatus:
            capacity=' Capacity'
        else:
            capacity=' Volumetric'

        if self.RTPStatus and not self.vFITStatus:
            self.optimizationState='RTP and Fixed FIT'+capacity
        elif self.RTPStatus and self.vFITStatus:
            self.optimizationState='RTP and Variable FIT'+capacity
        elif not self.RTPStatus and self.vFITStatus:
            self.optimizationState='Fixed Price and Variable FIT'+capacity
        logging.info("Real Time Pricing Optimization: Day {}, Time Duration {}, PV Size {} ".format(self.day,
                                                                                                    self.timeDuration,
                                                                                                    self.pvSize))
        # Getting Parameters
        wholesalepri = self.priceList
        pri = self.retailElectricityTariff
        load = self.loadList
        PV = self.pvGenList
        FeedIn = self.FIT
        optimization_duration=self.timeDuration

        model = Model("RTP_withForesight")  # Create Gurobi Model
        model.setParam('OutputFlag', 0)
        eStorage, ePVtoBatt, ePVtoLoad,ePVtoGrid,eBatttoGrid,eBatttoLoad,eGridtoLoad,eGridtoBatt = {}, {}, {}, {}, {}, {},{},{}# Intialize Constraint Dictionary
        y={}
        ''' All Efficiencies are taken with reference to the battery. if battery discharges 1kwh,
         this means it actually gives etaDischarge*1kwh to the grid...if battery charges by 1 kwh, this means it
          took 1/etacharge from the grid/pv
        '''

        for j in range(optimization_duration):  # creates variables along with lower and upper bounds
            y[j]=model.addVar(vtype='b')
            ePVtoBatt[j] = model.addVar(vtype='c', lb=0, ub=self.maximumChargeDischargeCapacity / self.chargeEfficiency, name="ePVtoBatt[%s]" % j)
            eBatttoLoad[j]=model.addVar(vtype='c', lb=0, ub=self.maximumChargeDischargeCapacity * self.dischargeEfficiency, name="eBatttoLoad[%s]" % j)
            ePVtoLoad[j]=model.addVar(vtype='c',lb=0,name="ePVtoLoad[%s]"%j)
            ePVtoGrid[j]=model.addVar(vtype='c',lb=0,name="ePVtoGrid[%s]"%j)
            eBatttoGrid[j] = model.addVar(vtype='c', lb=0,ub=self.maximumChargeDischargeCapacity*self.dischargeEfficiency, name="eBatttoGrid[%s]" % j)
            eGridtoBatt[j] = model.addVar(vtype='c', lb=0, ub=self.maximumChargeDischargeCapacity / self.chargeEfficiency, name="eGridtoBatt[%s]" % j)
            eStorage[j] = model.addVar(vtype='c', lb=0, ub=self.maximumBatteryCapacity, name="eStorage[%s]" % j)
            eGridtoLoad[j]=model.addVar(vtype='c',lb=0,name="eGridtoLoad[%s]"%j)

        model.update()

        model.setObjective(sum(eBatttoGrid[j]*wholesalepri[j] - eGridtoBatt[j]*pri[j]+FeedIn[j]*ePVtoGrid[j]-pri[j]*eGridtoLoad[j]
                               for j in range(optimization_duration)),
                           GRB.MAXIMIZE)

         # set objective function maximizing revenue


        for i in range(optimization_duration):  # Adding energy constraints for length of optimization_duration

            if i == 0:  # intial value
                model.addConstr(
                    eStorage[i] - self.initialBatteryCapacity * (1 - self.selfDischarge) - ePVtoBatt[i] * self.chargeEfficiency
                    -eGridtoBatt[i]*self.chargeEfficiency+eBatttoLoad[i]/self.dischargeEfficiency+eBatttoGrid[i]/self.dischargeEfficiency== 0)
            else:
                model.addConstr(eStorage[i] - eStorage[i-1] * (1 - self.selfDischarge) - ePVtoBatt[i] * self.chargeEfficiency -
                    eGridtoBatt[i] * self.chargeEfficiency + eBatttoLoad[i] / self.dischargeEfficiency + eBatttoGrid[i] / self.dischargeEfficiency == 0)

            model.addConstr(ePVtoLoad[i] + ePVtoBatt[i] +ePVtoGrid[i]  == PV[i])
            model.addConstr(eGridtoLoad[i]+eBatttoLoad[i]+ePVtoLoad[i]==load[i])
            model.addConstr(eBatttoGrid[i] <= self.maximumChargeDischargeCapacity *y[i]* self.dischargeEfficiency)
            model.addConstr(eGridtoBatt[i] <= self.maximumChargeDischargeCapacity * (1-y[i]) / self.chargeEfficiency)

        model.update()
        model.optimize()

        if model.status == GRB.Status.INF_OR_UNBD:
            # Turn presolve off to determine whether model is infeasible
            # or unbounded
            model.setParam(GRB.Param.Presolve, 0)
            model.optimize()
            print(model.status)
        # #extracting optimization results. Pretty ugly
        PVtoGrid, PVtoLoad,PVtoBatt,BatttoLoad,BatttoGrid,BatteryState,GridtoLoad,GridtoBatt = [], [], [], [], [],[],[],[]
        for i in range(optimization_duration):
            vars = model.getVarByName("ePVtoBatt[%s]" % i)
            PVtoBatt.append(vars.x)
            vars = model.getVarByName("eBatttoLoad[%s]" % i)
            BatttoLoad.append(vars.x)
            vars = model.getVarByName("ePVtoLoad[%s]" % i)
            PVtoLoad.append(vars.x)
            vars = model.getVarByName("ePVtoGrid[%s]" % i)
            PVtoGrid.append(vars.x)
            vars = model.getVarByName("eBatttoGrid[%s]" % i)
            BatttoGrid.append(vars.x)
            vars=model.getVarByName("eGridtoBatt[%s]"%i)
            GridtoBatt.append(vars.x)
            vars=model.getVarByName("eStorage[%s]"%i)
            BatteryState.append(vars.x)
            vars=model.getVarByName("eGridtoLoad[%s]"%i)
            GridtoLoad.append(vars.x)

        #
        # energyFromGrid = np.array(energyFromGrid)
        # energyToGrid = np.array(energyToGrid)
        # eDeltaGrid = energyFromGrid - energyToGrid

        #
        for i in range(self.timeDuration):
            if self.priceList[i]<0 and BatttoGrid[i]>0:
                logger2.warning('time: {} day: {} Load: {} PV:{} Batt:{} Policy:{} '.format(i,self.day,self.loadRow,
                                                                                            self.pvSize,self.maximumBatteryCapacity,
                                                                                            self.optimizationState))


        ans = DataFrame({"Prices": pri,
                         "load": load,
                         "PV": PV,
                         "Feed in":FeedIn,
                         'Battery State (kW)': BatteryState,
                         'Energy PV to Batt (kW)': PVtoBatt,
                         'Energy PV to Load (kW)': PVtoLoad,
                         'Energy PV to Grid (kW)': PVtoGrid,
                         'Energy Battery to Grid (kW)': BatttoGrid,
                         'Energy Battery to Load (kW)': BatttoLoad,
                         'Energy Grid to Load (kW)': GridtoLoad,
                         'Energy Grid to Batt (kW)': GridtoBatt
                         }, )
        # E_Direct_USE = 0
        # E_STORAGE_FROM_PV = 0
        # E_STORAGE_ = self.initialBatteryCapacity
        # for f in range(self.timeDuration):
        #     E_STORAGE_FROM_PV*=1
        #     E_Direct_USE+=PVtoLoad[f]
        #     if BatttoLoad[f] >E_STORAGE_FROM_PV:
        #         E_Direct_USE+=E_STORAGE_FROM_PV*1
        #         E_STORAGE_FROM_PV=0
        #     else:
        #         E_Direct_USE+=BatttoLoad[f]
        #         E_STORAGE_FROM_PV-=BatttoLoad[f]/1
        #     E_STORAGE_FROM_PV+=PVtoBatt[f]
        #
        #
        # self.directUse=E_Direct_USE

        ans = ans[['Prices', 'load', 'PV', 'Feed in', 'Battery State (kW)', 'Energy PV to Batt (kW)',
                                  'Energy PV to Load (kW)',
                                  'Energy PV to Grid (kW)', 'Energy Battery to Grid (kW)', 'Energy Battery to Load (kW)',
                                  'Energy Grid to Load (kW)', 'Energy Grid to Batt (kW)']]

        self.energyStorage=BatteryState #used for SFI
        self.sumEnergyFromGrid=np.array(GridtoLoad)+np.array(GridtoBatt) #used for avoided network costs
        self.revenue=(np.dot(self.FIT,PVtoGrid)+np.dot(self.priceList,BatttoGrid) - np.dot(self.retailElectricityTariff ,GridtoLoad)-
        np.dot(self.retailElectricityTariff,GridtoBatt))
        self.deltaBatt=sum(np.array(BatttoGrid)-np.array(GridtoBatt))
        self.PVtoGrid=sum(PVtoGrid)
        self.GridtoLoad=sum(GridtoLoad)
        self.referenceRevenue=np.dot(-self.retailElectricityTariff, self.loadList)

        ans = ans.round(3)
        return ans, model.objVal  # function returns results as DataFrame and the value of objective function


    def batteryCountsArb(self):
        tot = np.array(self.energyStorageArbitrage)
        tot1 = np.insert(tot, 0, self.initialBatteryCapacity)
        tot1 = np.delete(tot1, -1)
        diff = tot - tot1
        numOfCycles = 0
        z = 0
        for i in diff:
            z += abs(i)
            if z >= 2 * self.maximumBatteryCapacity:
                z = z - 2 * self.maximumBatteryCapacity
                numOfCycles += 1
        return numOfCycles
