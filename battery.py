
""" battery.py: Battery Class that imports and sets Battery parameters"""
import logging
import pandas as pd
from pandas import DataFrame
from paths import *
from input import import_PV

__author__ = 'Ahmad Ziade'
__email__ = 'ahmad.ziade@dlr.de'
__version__='1.0.1'
__credits__ = ["Martin Klein"]
__status__ = "Production"


__all__=['Battery']

class Battery(object):
    def __init__(self,path=None):
        self.__ratio=2
        if path==None:
            self.setDefaultParameters()
        else:
            self.path=path
            logging.info("Reading Parameters from ",self.path)
            self.setBattParametersFromFile(self.path)

    def setDefaultParameters(self):
        '''
        Sets default battery Parameters
        '''
        logging.info("Default Battery Parameters are set")
        self.selfDischarge = 0.002
        self.chargeEfficiency = .95
        self.dischargeEfficiency = .95
        self.__maximumBatteryCapacity = 4
        self.maximumChargeDischargeCapacity=self.maximumBatteryCapacity/self.ratio
        self.initialBatteryCapacity = 0

    def setBattParametersFromFile(self, path):
        '''
        sets battery parameters from csv file located in 'path'
        '''
        logging.info("Battery Parameters Set from {} ".format(path))

        parameters=read_Parameters(path)
        self.chargeEfficiency = parameters[1][0]
        self.dischargeEfficiency = parameters[1][1]
        self.selfDischarge =parameters[1][2]
        self.maximumChargeDischargeCapacity = parameters[1][4]
        self.initialBatteryCapacity = parameters[1][5]
        self.maximumBatteryCapacity = parameters[1][6]

    @property
    def ratio(self):
        ''' battery E2P ratio'''
        return self.__ratio
    @ratio.setter
    def ratio(self,r):

        self.__ratio=r
        self.__calculateMaximumChargeDischargeCapacity()

    @property
    def maximumBatteryCapacity(self):
        return self.__maximumBatteryCapacity

    @maximumBatteryCapacity.setter
    def maximumBatteryCapacity(self,m):
        self.__maximumBatteryCapacity=m
        self.__calculateMaximumChargeDischargeCapacity() #recalculates discharge and charge capacity


    def __calculateMaximumChargeDischargeCapacity(self):
        self.maximumChargeDischargeCapacity=self.maximumBatteryCapacity/self.ratio

    def getBatteryParameters(self):
        '''
        returns battery parameters as dataframe ( variables are list due to PANDAS bug of not accepting integers)
        '''
        data=DataFrame({"Charging Efficiency":[self.chargeEfficiency],
                        "Discharging Efficiency":[self.dischargeEfficiency],
                        "Self Discharge":[self.selfDischarge],
                        "Maximum Charging and Discharge Capacity":[self.maximumChargeDischargeCapacity],
                        "Maximum Battery Capacity":[self.maximumBatteryCapacity],
                        "Minimum Battery Capacity":[self.initialBatteryCapacity],
                        })
        return data
