
""" pv.py: PV class that imports generation data and sets PV parameters """
import logging
import pandas as pd
from pandas import DataFrame
from input import import_PV
from paths import *


__author__ = 'Ahmad Ziade'
__email__ = 'ahmad.ziade@dlr.de'
__version__='1.0.1'
__credits__ = "Martin Klein"
__status__ = "Production"



__all__=['PV']

class PV(object):
    def __init__(self,path=None):
        ''' If path not defined sets parameter from default values'''
        if path==None:
            logging.info("Default PV Parameters are set")
            self.setDefaultPvParameters()
        else:
            self.path=path
            self.setPvParametersFromFile(self.path)
        self.calculatePvGen()


    def setDefaultPvParameters(self):
        ''' Default parameters'''
        self.__pvSize=6   #kW
        self.__irradiation=1253 #kwh/ae
        self.__performanceRatio=.84
        self.__gamma=.9

    def setPvParametersFromFile(self,path):
        ''' reads PV parameter from file in Path and updates attributes'''
        try:
            pvParameter=read_Parameters(path)
            logging.info("PV Parameters Set From {}".format(path))
            self.pvSize=pvParameter[1][0]
            self.irradiation=pvParameter[1][1]
            self.performanceRatio=pvParameter[1][2]
            self.gamma=pvParameter[1][3]
        except:
            logging.WARNING("Error with setting PV parameter from file")

    def calculatePvGen(self,pvList=import_PV()):
        ''' calculates PvGen based on PV parameters'''
        self.calculatedPvGen=pvList*self.__pvSize * self.__irradiation * self.__gamma * self.__performanceRatio
        logging.info("PV Generation for PV Size {} kW is calculated".format(self.pvSize))

    #method attributes, calculated PV is updated when attributes are updated
    @property
    def pvSize(self):
        return self.__pvSize

    @pvSize.setter
    def pvSize(self,PV):
        self.__pvSize=PV
        self.calculatePvGen()


    @property
    def irradiation(self):
        return self.__irradiation
    @irradiation.setter
    def irradiation(self,irr):
        self.__irradiation=irr
        self.calculatePvGen()

    @property
    def performanceRatio(self):
        return self.__performanceRatio

    @performanceRatio.setter
    def performanceRatio(self,perform):
        self.__performanceRatio=perform
        self.calculatePvGen()

    @property
    def gamma(self):
        return self.__gamma
    @gamma.setter
    def gamma(self,gam):
        self.__gamma=gam
        self.calculatePvGen()


    def getPvParameters(self):
        data=DataFrame({'Pv Size':self.pvSize,
                        'Irradiation':self.irradiation,
                        'Performance Ratio':self.performanceRatio,
                        'Gamma':self.gamma})


