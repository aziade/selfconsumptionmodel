from paths import *
from optimize import *
from pandas import DataFrame
import pylab
import logging
import pandas as pd
import numpy as np
import math
import irr
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.rcsetup as RCSETUP
from scipy import stats

__all__=['Economic']

class Economic(Optimizer):

    def __init__(self,path=None):
        Optimizer.__init__(self)
        if path==None:
            self.setDefaultEconomicParameters()
        else:
            self.path=path
            self.setEconomicParametersFromFile(self.path)



    def setDefaultEconomicParameters(self):
        self.discount=.05
        self.lifetime=20
        self.pv_invest=1500 #Eur/kW
        self.batt_invest=1500 #EUr/kWh
        self.pv_OM=.01 #1/a
        self.batt_OM=.01 #1/a
        self.VAT=1.19

    def setEconomicParametersFromFile(self,path):
        ''' reads economic parameters from file'''
        logging.info("Economic Parameters Set From {}".format(path))
        parameters = read_Parameters(path)
        self.discount=parameters[1][0]
        self.lifetime=parameters[1][1]
        self.pv_invest=parameters[1][2]
        self.batt_invest=parameters[1][3]
        self.pv_OM=parameters[1][4]
        self.batt_OM=parameters[1][5]
        self.VAT=parameters[1][6]

    def __pvInitialCost(self):
        ''' Calculates initial cost of PV '''
        if self.pvSize==0:
            initialCostperKW=0
        else:
            initialCostperKW=self.pv_invest*(self.pvSize/10)**-0.063
        initialCostPV=self.pvSize*initialCostperKW
        return initialCostPV
    def __batteryInitialCost(self):
        ''' Calculates initial cost of Battery '''
        if self.maximumBatteryCapacity==0:
            initialCostBattery=0
        else:
            initialCostperKW = self.batt_invest * (self.maximumBatteryCapacity / 10) ** -0.154
        initialCostBattery = self.maximumBatteryCapacity * initialCostperKW
        return initialCostBattery

    @property
    def avoidedNetworkFees(self):
        self.calculateAvoidedNetworkFees()
        return self.__avoidedNetworkFees

    def calculateAvoidedNetworkFees(self):
        ''' calculates avoided network fees'''
        if self.capacityStatus: #all capacity-based network charges are conserved
            self.__avoidedNetworkFees = 0
        elif self.optimizationStatus==1: #BAU
            selfProduced=self.loadList-self.energyFromGrid
            self.__avoidedNetworkFees=sum(selfProduced)*self.networkCharge
        elif self.optimizationStatus==2:#Non-BAU
            selfProduced=self.loadList-self.sumEnergyFromGrid
            self.__avoidedNetworkFees=sum(selfProduced)*self.networkCharge

    def optimizeYear(self):
        self.revenueTotal=0
        self.referenceTotal=0
        self.batteryTotal=[]
        self.pvTotal=0
        self.fedin=0
        self.fromGrid=0
        self.consumptionYear=0
        self.totalDirectUse=0
        self.totalAvoidedNetworkFees=0 #TODO totalAvoidedNetworkFees can be called before optimize year, fix that
        self.maximumBATT=0
        self.deltaBattGrid=[]
        if not self.RTPStatus and not self.vFITStatus: #BAU Case
            self.timeDuration=8760
            self.optimize()
            self.revenueTotal=self.revenue
            self.referenceTotal=self.referenceRevenue
            self.totalAvoidedNetworkFees=self.avoidedNetworkFees
            self.batteryTotal=self.energyStorage
            self.pvTotal=sum(self.pvGenList)
            # self.totalDirectUse=self.directUse
            self.consumptionYear=sum(self.loadList)
        else:
            for d in range(1,366):
                self.timeDuration=24
                self.day=d
                self.optimize()
                self.deltaBattGrid.append(self.deltaBatt)
                self.fedin+=self.PVtoGrid
                self.fromGrid+=self.GridtoLoad
                self.revenueTotal+=self.revenue
                self.referenceTotal+=self.referenceRevenue
                self.batteryTotal+=(self.energyStorage)
                self.totalAvoidedNetworkFees+=self.avoidedNetworkFees
                # self.totalDirectUse+=self.directUse
                self.pvTotal+=sum(self.pvGenList)
                self.consumptionYear+=sum(self.loadList)

                # self.totalSFI.append(self.SFI(isYear=False)) #saves all SFI values
            self.energyStorage=self.batteryTotal
        self.numOfCycles=self.batteryCounts()
        self.revenueTotal-=self.fixedCapacity
        self.referenceTotal-=self.fixedCapacity


    def calculateNPV(self,discount=None):
        if discount==None:
            discount=self.discount
        PV=self.__pvInitialCost()
        Batt=self.__batteryInitialCost()
        cost=(-PV-Batt)*self.VAT #initial investment year0
        logging.info('Net Present Value for {} years calculated'.format(self.lifetime))
        batteryYear = 8000 / self.numOfCycles  # number of years to change battery. Assumed life span 8000
        batteryYear=int(batteryYear)
        for year in range(1,int(self.lifetime+1)):
            refYearCost=self.referenceTotal/math.pow(1.0+discount,year)
            if year==batteryYear:
                logging.warning('Battery is Changed')
                yearCost=self.revenueTotal-self.pv_OM*PV-Batt*self.VAT*.25
            else:
                yearCost=self.revenueTotal-self.pv_OM*PV-self.batt_OM*Batt
            yearCost=yearCost/math.pow(1.0+discount,year)
            cost+=yearCost-refYearCost
        return cost

    def calculateIRR(self):
        PV = self.__pvInitialCost()
        Batt = self.__batteryInitialCost()
        initialCost=(-PV-Batt)*self.VAT #initial investment year0
        yearlyCost = self.revenueTotal - self.pv_OM*PV - self.batt_OM *Batt
        li=[initialCost]+[yearlyCost-self.referenceTotal]*self.lifetime
        batteryYear=8000/self.numOfCycles #number of years to change battery. Assumed life span 8000
        batteryYear=int(batteryYear)
        if batteryYear<=self.lifetime:
            logging.warning('Battery is Changed')
            li[batteryYear]=self.revenueTotal - self.pv_OM*PV-Batt*self.VAT*.25 -self.referenceTotal#at battery year, the cost of
        ###PRETTY UGLY BUT MAKES SURE IRR WON'T Diverge
        ir=irr.irr_newton(li)
        if ir==-1:
            ir=irr.irr_binary_search(li)
            if ir==-1:
                ir=np.irr(li)
                if np.isnan(ir)==True:
                    ir=-1
        return ir

    def batteryCounts(self):
        '''
        calculates
        '''
        tot=np.array(self.batteryTotal)
        tot1=np.insert(tot,0,self.initialBatteryCapacity)
        tot1=np.delete(tot1,-1)
        diff=tot-tot1
        numOfCycles = 0
        z = 0
        for i in diff:
            z += abs(i)
            if z >= 2 * self.maximumBatteryCapacity:
                z = z - 2 * self.maximumBatteryCapacity
                numOfCycles += 1
        return numOfCycles


#     print(Si)

    # en=[]
    # for x in np.arange(0,10,1):
    #     w.maximumChargeDischargeCapacity=x
    #     w.maximumChargeDischargeCapacity=x
    #     w.optimizeYear()
    #     en.append(w.SFI(isYear=True))
    #     print(x)
    #
    # plt.figure()
    # plt.plot(en,'b')
    # plt.xlabel('Charge/Discharge Capacity')
    # plt.ylabel('SFI')
    # plt.show()

    # w1.timeDuration=8760
    # w1.optimizeArbitrage()
    # stor2=w1.energyStorageArbitrage
    # diff=np.array(stor2)-np.array(stor)
    # diff=diff.astype(np.int)
    # diff=diff[diff!=0]
    # print(diff)
    # print(len(diff))
    # plt.figure()
    # plt.plot(diff)
    # plt.show()




# def func():
#     w=Economic()
#     w1=Economic()
#     w1.RTPStatus=False
#     list=[]
#     for pv in [.5,2.5,5,10]:
#         w.pvSize=pv
#         w1.pvSize=pv
#         for batt in [.5,2.5,5,10]:
#             w.maximumBatteryCapacity=batt
#             w1.maximumBatteryCapacity=batt
#             w.optimizeYear()
#             w1.optimizeYear()
#             print((pv,batt,w.totalAvoidedNetworkFees))
#             print((pv,batt,w1.totalAvoidedNetworkFees))
#
#
#
#
#
#
#
#     #
#     #
#     # sfiDataFrame = DataFrame()
#     # sfiDataFrame1=DataFrame()
#     # irrDataFrame=DataFrame()
#     # irrDataFrame1=DataFrame()
#
#
#
#     # for pv in [1,3,5,7.5,10]:
#     #     print("PV:",pv)
#     #     w.pvSize=pv
#     #     w1.pvSize=pv
#     #     for batt in [1,2,3,4,5,7.5,10]:
#     #         print("Batt :",batt)
#     #         w.maximumBatteryCapacity=batt
#     #         w1.maximumBatteryCapacity=batt
#     #         for l in range(0, 10):
#     #             w.loadRow = l
#     #             w1.loadRow=l
#     #             w.optimizeYear()
#     #             w1.optimizeYear()
#     #             irr=w.calculateIRR()
#     #             irr1=w1.calculateIRR()
#     #             df = DataFrame(
#     #                 {'PV Size': [w.pvSize], 'Load Row': [w.loadRow], 'Battery Size': [w.maximumBatteryCapacity],
#     #                  'IRR': [irr]})
#     #             df1=DataFrame(
#     #                 {'PV Size': [w1.pvSize], 'Load Row': [w1.loadRow], 'Battery Size': [w1.maximumBatteryCapacity],
#     #                  'IRR': [irr1]})
#     #             irrDataFrame = pd.concat([df, irrDataFrame])
#     #             irrDataFrame1=pd.concat([df1,irrDataFrame1])
#     #             li = w.totalSFI
#     #             li1=w1.totalSFI
#     #             se = pd.Series(li)
#     #             se1=pd.Series(li1)
#     #             sfiDataFrame[str(w.loadRow)] = se.values
#     #             sfiDataFrame1[str(w1.loadRow)]=se1.values
#     #
#     #         sfiDataFrame.to_excel('Results/SFI/%s_PV %s_Batt%s_SFI.xlsx' % (
#     #         w.optimizationState, w.pvSize, w.maximumBatteryCapacity))
#     #         sfiDataFrame1.to_excel('Results/SFI/%s_PV %s_Batt%s_SFI.xlsx' % (
#     #         w1.optimizationState, w1.pvSize, w1.maximumBatteryCapacity))
#     # irrDataFrame.to_excel("Results/%s_%s Load Rows_IRR.xlsx" % (w.optimizationState, w.loadRow))
#     # irrDataFrame1.to_excel("Results/%s_%s Load Rows_IRR.xlsx" % (w.optimizationState, w.loadRow))






