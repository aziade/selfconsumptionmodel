import os.path
import pandas as pd
import csv
import logging
#
#


""" paths.py: Sets the Paths of the input data """

__author__ = 'Ahmad Ziade'
__email__ = 'ahmad.ziade@dlr.de'
__version__='1.0.1'
__credits__ = ["Martin Klein"]
__status__ = "Production"


logging.basicConfig(filename='logger.log',filemode='w',level=logging.CRITICAL, format=' %(asctime)s -  %(levelname)s-  %(message)s')
logging.disable(logging.CRITICAL)
# logger2=logging.basicConfig(filename='logger2',filemode='w',level=logging.DEBUG, format=' %(asctime)s -  %(levelname)s-  %(message)s')




formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
path_ideal_GAMS='Scripts_GAMS/ideal.gms'
path_RTP_GAMS='Scripts_GAMS/RTP.gms'
path_Prices='Data/Market_Data_2016.csv'
path_Load= 'Data/summedLoadProfiles.csv'
path_PvGen='Data/genData.csv'
path_BattParmeters='Parameters/battery_Parameters.csv'
path_PvParameters='Parameters/pv_parameters.csv'
path_EconParameters='Parameters/economic_parameters.csv'


def set_Path(PATH):
    scriptdir = os.path.dirname(os.path.abspath(__file__))  # returns working directory of the current folder
    path=os.path.join(scriptdir,PATH)
    return str(path)

def read_Parameters(path):
    path=set_Path(path)
    try:
        parameters = pd.read_csv(path,header=None)
        return parameters
    except IOError:
        print("I/O Error, Unable to retrieve data from {}".format(path))



def get_model_text(PATH=path_ideal_GAMS):
    ''' reads GAMS script as string '''
    path = set_Path(PATH)
    file = open(str(path), "r")
    data = file.read()
    return data

def to_Dict(list):
    ''' changes input to GAMS-friendly dict '''
    l = []
    for i in range(1, len(list) + 1):
        l.append("t%s" % i)
    dictionary = dict(zip(l, list))
    return dictionary
def to_Dict2(list):
    ''' changes input to GAMS-friendly dict '''
    l = []
    for i in range(0, len(list)):
        day=int(i/24)+1
        time = (i+1)-((day-1)*24)
        day='dd%s'%day
        time="t%s"%time
        l.append((day,time))
    dictionary = dict(zip(l, list))
    return dictionary




def setup_logger(name, log_file, level=logging.INFO):
    """Function setup as many loggers as you want"""

    handler = logging.FileHandler(log_file)
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger

logger2=setup_logger('logger2','second_logger.log')