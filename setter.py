from __future__ import print_function
from pv import *
from paths import *
import pandas as pd
import numpy as np
import logging
from input import *

""" setter.py: InputSetter Class inherits from PV and imports and sets duration fro market prices, PV and load """

__author__ = 'Ahmad Ziade'
__email__ = 'ahmad.ziade@dlr.de'
__version__='1.0.1'
__credits__ = ["Martin Klein"]
__status__ = "Production"


pd.set_option('display.expand_frame_repr', False) # increase width of dataframe print statement


__all__=['totalPrices','totalLoad','totalPvGen','InputSetter']


totalPrices=import_Prices(path_Prices)
totalPvGen=import_PV(path_PvGen)
totalLoad=import_Load(path_Load)


class InputSetter(PV):

    def __init__(self,):
        PV.__init__(self)
        self.__timeDuration = None
        self.__day = None
        self.__loadRow=None
        self.totalPrices=totalPrices
        self.totalPvGen=totalPvGen
        self.totalLoad=totalLoad
        self.totalAverageLoad=totalLoad.mean(axis=1)
        self.optimizationStatus=None

    def loadLoadCurveFromFile(self, relativePath=None):
        if relativePath==None:
            relativePath=self.loadPath
        try:
            absPath=set_Path(relativePath)
            self.totalLoad=pd.read_csv(absPath, header=None, delimiter=';')
            logging.info("load Successfully Imported from {}".format(relativePath))
        except:
            raise IOError("load Input from {} Error".format(relativePath))

    def loadPvGenFromFile(self, relativePath=None):
        if relativePath==None:
            relativePath=self.pvPath
        try:
            absPath=set_Path(relativePath)
            self.totalPvGen=pd.read_csv(absPath,header=None)
            logging.info("PV Gen Successfully Imported from {}".format(relativePath))
        except:
            raise IOError("Pv Gen Input from {} Error".format(relativePath))

    def loadPricesFromFile(self, relativePath=None):
        if relativePath==None:
            relativePath=self.pricePath
        try:
            absPath=set_Path(relativePath)
            self.totalPrices=pd.read_csv(absPath,sep='\t')
            logging.info("Prices Successfully Imported from {}".format(relativePath))
        except:
            raise IOError("Price Input from {} Error ".format(relativePath))


    def PricesListGet(self, day=None, duration=None):
        ''' returns price list as np.array for specified day and duration'''
        if day==None:
            day=self.day
            logging.debug("Day {} is set".format(day))
        if duration==None:
            duration=self.timeDuration
            logging.debug("Time Series Duration {} is set".format(day))
        if day==None or duration==None:
            raise ValueError("Please Specify a day and time series duration")
        try:
            hours = (day - 1) * 24
            price = np.array(self.totalPrices['Price'][hours:hours + duration])

            if np.nan in price:
                raise IOError
            return price / 1000 # in kWh

        except IOError:
            logging.WARNING('Price list in day {} contains a missing values '.format(day))

    def LoadListGet(self, day=None, duration=None, loadRow=None):
        if day == None:
            day = self.day
        if duration == None:
            duration = self.timeDuration
        if loadRow == None:
            loadRow = self.loadRow

        if day == None or duration == None or loadRow == None:
            raise ValueError("Please set day, duration and load Row")

        if loadRow==100: ## LOAD ROW 100 gives average load row
            hours = (day - 1) * 24
            load = np.array(self.totalAverageLoad[:][hours:hours + duration])
            return load / 1000  # kWh
        else:
            hours = (day - 1) * 24
            load = np.array(self.totalLoad[loadRow][hours:hours + duration])
            return load/1000 #kWh

    def PvGenListGet(self, day=None, duration=None):
        if day==None:
            day=self.day
        if duration==None:
            duration=self.timeDuration
        if day==None or duration==None:
            raise ValueError("Please Specify a day and time series duration")
        hours = (day - 1) * 24
        result = np.array(self.calculatedPvGen[0][hours:hours + duration])
        return result/1000 #kW

    @property
    def pvGenList(self):
        return self.PvGenListGet()
    @property
    def priceList(self):
        return self.PricesListGet()
    @property
    def loadList(self):
        return self.LoadListGet()

    @property
    def timeDuration(self):
        return self.__timeDuration
    @timeDuration.setter
    def timeDuration(self,time):
        self.__timeDuration=time

    @property
    def day(self):
        return self.__day
    @day.setter
    def day(self,day):
        self.__day=day

    @property
    def loadRow(self):
        return self.__loadRow
    @loadRow.setter
    def loadRow(self,loadrow):
        logging.info('Load Row Changed to {}'.format(loadrow))
        self.__loadRow=loadrow
