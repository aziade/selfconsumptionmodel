from paths import *
from economic_Assessment import *
from pandas import DataFrame
import pylab
import logging
import timeit
import pandas as pd
import numpy as np
import math
import irr
import matplotlib
from SALib.sample import saltelli
from SALib.analyze import sobol
import matplotlib.pyplot as plt
import matplotlib.rcsetup as RCSETUP
import matplotlib
from scipy import stats


__all__=['Model']


class Model(Economic):

    def __init__(self):
        Economic.__init__(self)
    #
    # @property
    # def selfConsumption(self):
    #     return self.totalDirectUse/self.pvTotal
    @property
    def selfConsumption(self):
        if self.optimizationStatus==1: #BAU
            return 1-sum(self.energyToGrid)/self.pvTotal
        else:
            return 1-(self.fedin+max(sum(self.deltaBattGrid),0))/self.pvTotal

    # @property
    # def autarky(self):
    #     return self.totalDirectUse/self.consumptionYear
    @property
    def autarky(self):
        if self.optimizationStatus == 1:  # BAU
            return 1-sum(self.energyFromGrid)/self.consumptionYear
        else:
            return 1-(self.fromGrid-min(sum(self.deltaBattGrid),0))/self.consumptionYear

    def SFIYearAllLoadRow(self,loadRows=None):
        '''
        Takes loadrow as optional input
        exports system-friendliness indicator to excel file for all load rows for each day of year
        returns dataframe containing SFI.
        '''
        if loadRows==None:
            loadRows=74
        sfiDataFrame = DataFrame()

        for l in range(0, loadRows):
            self.loadRow = l
            self.optimizeYear()
            li = self.totalSFI
            se = pd.Series(li)
            sfiDataFrame[str(self.loadRow)] = se.values
        sfiDataFrame.columns.names = ['Load Row']
        print(sfiDataFrame)
        sfiDataFrame.to_excel('Results/SFI/%s_PV %s_Batt%s_SFI.xlsx' % (self.optimizationState, self.pvSize,self.maximumBatteryCapacity))
        return sfiDataFrame

    def plotStorage(self,isYear):
        ''' plots storage charging state vs ideal case. Takes isYear as boolean parameter
        isYear is true if optimized  for year False if only for day '''
        plt.figure()
        if isYear:
            x=range(0,8760)
            y1=self.calculateBatteryStateBinary(self.arbitrageBatteryTotal)
            y2=self.calculateBatteryStateBinary(self.batteryTotal)

        else:
            x=range(0,23)
            y1=self.calculateBatteryStateBinary(self.energyStorageArbitrage)
            y2=self.calculateBatteryStateBinary(self.energyStorage)
        plt.plot(y1,'k',label='Arbitrage')
        plt.plot(y2,'b',label='Dispatch')
        plt.title('Dispatch {}'.format(self.optimizationState))
        plt.ylabel('Battery Storage State')
        plt.xlabel('Time (h)')
        pylab.legend(loc='upper right')
        print('Showing Plot')

    def SFI(self):
        ''' calculates system friendliness indicator. After optimization of existing case the arbitrage case is optimized for entire year'''
        self.day=1
        self.timeDuration=8760
        self.optimizeArbitrage()
        if self.optimizationStatus is None:
            self.optimize()
        if self.arbitrageState is None:
            self.optimizeArbitrage()
        arbitrageCharging = self.calculateBatteryStateBinary(self.energyStorageArbitrage)
        optimizeCharging = self.calculateBatteryStateBinary(self.energyStorage)
        logging.info(
            'System Friendliness Indicator for Arbitrage and {} calculated '.format(self.optimizationState))
        try:
            assert len(arbitrageCharging)==len(optimizeCharging)
            diff = 1 - sum(
                (arbitrageCharging[i] - optimizeCharging[i]) ** 2 for i in range(len(arbitrageCharging))) / (
                           2 * len(arbitrageCharging))
        except AssertionError:
            raise Exception("Arbitrage and Optimized Lists are not the same length")
        return diff

    def SFIDay(self):
        if self.optimizationStatus is None:
            self.optimize()
        if self.arbitrageState is None:
            self.optimizeArbitrage()
        arbitrageCharging = self.calculateBatteryStateBinary(self.energyStorageArbitrage)
        optimizeCharging = self.calculateBatteryStateBinary(self.energyStorage)
        logging.info(
            'System Friendliness Indicator for Arbitrage and {} calculated '.format(self.optimizationState))
        try:
            assert len(arbitrageCharging) == len(optimizeCharging)
            diff = 1 - sum(
                (arbitrageCharging[i] - optimizeCharging[i]) ** 2 for i in range(len(arbitrageCharging))) / (
                           2 * len(arbitrageCharging))
        except AssertionError:
            raise Exception("Arbitrage and Optimized Lists are not the same length")
        return diff


    def pvfunc(self,tup):
        self.pvSize=tup[0]
        self.batt=tup[1]
        print(self.pvSize)
        self.optimizeYear()
        return self.SFI()








